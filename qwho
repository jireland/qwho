#! /exports/igmm/software/pkg/el7/apps/perl/5.24.0/bin/perl
# /usr/bin/env perl
#
# qwho - show who is using the IGC Eddie nodes along with the amout of resource 
# allocated (core, memory, run-time).
#
# John Ireland - UoE IGC - Mon 17 May 13:26:01 BST 2021

use XML::Simple;
use Data::Dumper;
use Number::Bytes::Human qw(format_bytes);
use Number::Format qw(:subs);
use Getopt::Std;
use Storable;

# Default memory per core - should be obtained from qconf call
$DefaultMemPerCore = "1G"; 

$Version = "0.0.4";
$Author = "John Ireland - UoE IGC";

if (!getopts('VabcmMrgGiuUsfhvtDwWqQn'))  {
        system "pod2usage", $0;
        exit 1;
} 
if ($opt_v) {
        print "$prog version $Version\n$Author\n";
        exit 0;
}

if (!$opt_c && !$opt_m && !$opt_M && !$opt_r && !$opt_t && !$opt_w && !$opt_W && !$opt_n && !$opt_M && !$opt_V) {
       # Use default: show cores 
       $opt_c = 1;
}

if ($opt_M) {
    $opt_m = 1;
}

if (!$opt_s && !$opt_i && !$opt_b) {
	$opt_b = 1;
        #print "Show Batch!\n";
}

$opt_w++ if ($opt_q);
$opt_W++ if ($opt_Q);

if ($opt_h) {
        system "pod2text", $0;
        exit 0;
}

# create XML object
$xml = new XML::Simple;

my $qwhodir = $ENV{"HOME"} . "/.qwho"; # per-User Cache Directory

my $DATA = "data";  # Name of data file 
$DATA = "data-a" if $opt_a;
$DATA = "data-u" if $opt_u;
$DATA = "data-V" if $opt_V;


if (! -d $qwhodir) {
    mkdir $qwhodir or die "Can\'t create $qwhodir\n";
}

# create XML file by running 'qstat -xml'
# check for existence, age, of cache and -f flag before creating
$age = (-M "$qwhodir/$DATA.xml") * 86400;
$CacheTTL = 300;

if ($opt_G) {
   # if verbose graphical mode switch on graphical mode
   $opt_g = 1
}

if ($opt_U) {
  # get the UUNs of IGC/IGMM users 
  open(USERS,   "qconf -su igmm-cluster |") || die("can't open input: $!");
  while (<USERS>) {
   $_ =~ s/[ \\]//g; 
   $_ =~ s/^name|^igmm-cluster|^type|^fshare|^oticket|^entries//;
   #print "$_\n";
   @i = split(/,/,$_); 
   $igmmusers{$_}++ for (@i);
  }
}

if ($opt_a) {
	system ("qstat  -u '*'  -r -xml > $qwhodir/$DATA.xml") if (!-e "$qwhodir/$DATA.xml" || $age > $CacheTTL || $opt_f);
} elsif ($opt_u) {
       #system ("qstat  -u '*'  -q *\@\@uoe_hosts -r -xml | sed 's/job ids/job_ids/' > $qwhodir/$DATA.xml") if (!-e "$qwhodir/$DATA.xml" || $age > $CacheTTL || $opt_f);
       system ("qstat  -u '*'  -q eddie -r -xml | sed 's/job ids/job_ids/' > $qwhodir/$DATA.xml") if (!-e "$qwhodir/$DATA.xml" || $age > $CacheTTL || $opt_f);
} elsif ($opt_V) {
      if ($opt_w || $opt_W) {
        # Queued GPU jobs/tasks are only held in the All data-a.xml file, while running GPU jobs are in data-V.xml
        $DATA = "data-a"; 
	system ("qstat  -u '*'  -r -xml > $qwhodir/$DATA.xml") if (!-e "$qwhodir/$DATA.xml" || $age > $CacheTTL || $opt_f);
      } else {
	system ("qstat  -u '*'  -q gpu -r -xml | sed 's/job ids/job_ids/' > $qwhodir/$DATA.xml") if (!-e "$qwhodir/$DATA.xml" || $age > $CacheTTL || $opt_f);
      }
} else {
	system ("qstat  -u '*'  -q *\@\@igmm_384G_32s -r -xml > $qwhodir/$DATA.xml") if (!-e "$qwhodir/$DATA.xml" || $age > $CacheTTL || $opt_f);
}

# read XML file into memory (converting to complex hash/array data-structure)
if (! -e "$qwhodir/$DATA.sto" || $age > $CacheTTL || $opt_f) {
     $data = $xml->XMLin("$qwhodir/$DATA.xml");
     # Save a fast-readable cache!
     store($data, "$qwhodir/$DATA.sto");
} else {
     # read fast-readable cache
     $data = retrieve("$qwhodir/$DATA.sto");
}

# Dump Perl complex hash/array data structures to a "data.dump" file in .qwho (readable)
# Useful for debuging and development
if ($opt_D) {
 $DUMP = "> $qwhodir/$DATA.dump";
 open DUMP or die "Can't open: $qwhodir/$DATA.dump: $!";
 print DUMP Dumper($data);
 close DUMP;
}

# Create XML file holding node total resources available on each node (qstat -F) - memory & cores 
$nodeResources = new XML::Simple;
$age = (-M "$qwhodir/$nodeResources.xml") * 86400;
$nodeResourcesCacheTTL = 86400;
if ($opt_g) {
   system ("qstat -F mem_total,m_core,hostname -xml  > $qwhodir/nodeResources.xml") if (!-e "$qwhodir/nodeResources.xml" || $age > $nodeResourcesCacheTTL || $opt_f);

  if (! -e "$qwhodir/nodeResources.sto" || $age > $nodeResourcesCacheTTL || $opt_f) {
     $nodeResources = $xml->XMLin("$qwhodir/nodeResources.xml");
     #Save a fast-readable cache!
     store($nodeResources, "$qwhodir/nodeResources.sto");
  } else {
      # read fast-readable cache
      $nodeResources = retrieve("$qwhodir/nodeResources.sto");
  }
  if ($opt_D) {
     $DUMP = "> $qwhodir/nodeResources.dump";
     open DUMP or die "Can't open: $qwhodir/nodeResources.dump: $!";
     print DUMP Dumper($nodeResources);
     close DUMP;
  }

  foreach $i (keys %{$nodeResources->{queue_info}->{'Queue-List'}}) {
     #print "$nodeResources->{queue_info}->{'Queue-List'}{$i}->{'resource'}->{'mem_total'}->{'content'}\n";
     #print "$nodeResources->{queue_info}->{'Queue-List'}{$i}->{'resource'}->{'m_core'}->{'content'}\n";
     $node = $i;
     $node =~ s/.*node//;
     $node =~ s/.*\@//;
     $node =~ s/\..*//;
     $MaxCore{$node} = $nodeResources->{queue_info}->{'Queue-List'}{$i}->{'resource'}->{'m_core'}->{'content'};
     $MaxMem{$node} = $nodeResources->{queue_info}->{'Queue-List'}{$i}->{'resource'}->{'mem_total'}->{'content'};
     #print "$node\n";
  }
}

# Process each job in the queue
foreach $i ($data->{queue_info}->{job_list}) {
      if (ref $i eq 'HASH') {
        $i = [$i];
      }
	foreach $j (@$i) {
          next if (! defined($igmmusers{$j->{JB_owner}}) && $opt_U);
          next if ( $j->{JB_name} =~ /QLOGIN/ && !$opt_i); # skip interactive jobs
	  next if ( $j->{queue_name} =~ /staging/ && !$opt_s); # skip staging jobs
          next if (!($j->{JB_name} =~ /QLOGIN/ || $j->{queue_name} =~ /staging/) && ! $opt_b); # skip batch jobs if not -b
	  if ( $j->{requested_pe}->{name} eq "mpi" || $j->{requested_pe}->{name} =~ /scatter/ || $opt_n || $opt_M) {
            # Need to process MPI and scatter jobs later seperately to find where resource is being used
            # because these jobs can be spread accoross Eddie
	    # We use a hash because GE double counts jobs on multiple nodes!
	    # If -n or -M ALL jobs are looked at to determine ioops, maxvmem etc. 
	    # So %MPIjobs will contain ALL jobs %MPIjobs will contain ALL jobs
	    $MPIjobs{$j->{JB_job_number}}++;
	    next;
	  } 
	  $slots = defined $j->{slots} ? $j->{slots} : 1;
          $UsersWithRuningJobs{$j->{JB_owner}}++;
	  if (defined $j->{hard_request}->{h_vmem}->{content}) {
             $j->{hard_request}->{h_vmem}->{content} =~ tr/mg/MG/;
             $MemByUser{$j->{JB_owner}} += (unformat_number($j->{hard_request}->{h_vmem}->{content}) * $slots); 
             $MemByNode{$j->{queue_name}} += (unformat_number($j->{hard_request}->{h_vmem}->{content}) * $slots);
          } else {
             # Default memory per core
             $MemByUser{$j->{JB_owner}} += unformat_number($DefaultMemPerCore)  * $slots;
             $MemByNode{$j->{queue_name}} += unformat_number($DefaultMemPerCore)  * $slots;
          }
          if (defined $j->{hard_request}->{h_rt}->{content}) {
             $RtByUser{$j->{JB_owner}} += $j->{hard_request}->{h_rt}->{content} * $slots;
             $RtByNode{$j->{queue_name}} += $j->{hard_request}->{h_rt}->{content} * $slots;
          }
          if ( $j->{requested_pe}->{name} eq "gpu-a100") {
		$GPUbyUser{$j->{JB_owner}} += $j->{requested_pe}->{content}
          } elsif ($j->{hard_request} eq "gpu" || $j->{queue_name} =~ /gpu/) {
                $GPUbyUser{$j->{JB_owner}}++;
          }

          $CoreByUser{$j->{JB_owner}} += $slots;
          $CoreByNode{$j->{queue_name}} += $slots;
          #if ($opt_M) {
            # Construct an XML file for each job to find current maxvmem
            #$xml = getjobXML($j->{JB_job_number});
	    #print $xml;
          #}
	}
}

# Check for any MPI jobs - note if -n or -M selected we will look at all jobs ie %MPIjobs will contain all jobs
foreach $mpi (keys %MPIjobs) {
  # print "mpi = $mpi\n";
  # Construct an XML file for each MPI job
  $age = (-M "$qwhodir/$mpi.xml") * 86400;
    system ("qstat  -j $mpi -xml | sed 's/job ids/job_ids/' > $qwhodir/$mpi.xml") if (!-e "$qwhodir/$xml.xml" || $age > $CacheTTL || $opt_f);
 
  if (!-e "$qwhodir/$mpi.sto" || $age > $CacheTTL || $opt_f) {
     $mpidata = $xml->XMLin("$qwhodir/$mpi.xml");
     # write Perl Data Structure (binary Storable file)
     store($mpidata, "$qwhodir/$mpi.sto");
  } else {
     $mpidata = $xml->XMLin("$qwhodir/$mpi.xml"); 
  }
   
  # print $mpidata->{queue_info}->{job_list};
  # Dump Perl mpidata structures
  if ($opt_D) {
     $DUMP = "> $qwhodir/$mpi.dump";
     open DUMP or die "Can't open: $qwhodir/$mpi.dump: $!";
     print DUMP Dumper($mpidata); 
     close DUMP;
  }
  $JB_owner = $mpidata->{djob_info}->{element}->{JB_owner};

 #
 # We are OK if {element} is an arrays of hashes (then the MPI job has spread out accross multiple eddie nodes)! 
 # Need to cope with {element} being a single hash reference (this happens if the MPI job is running on a single node)
 # To fix, we just wrap the hash ref in an array ref
 # that is: if ref $VAR == HASH then $VAR = [$VAR]
 if (ref $mpidata->{djob_info}->{element}->{JB_ja_tasks}->{element} eq 'HASH') {
  $mpidata->{djob_info}->{element}->{JB_ja_tasks}->{element} = [$mpidata->{djob_info}->{element}->{JB_ja_tasks}->{element}]
 }
 foreach $i (@{$mpidata->{djob_info}->{element}->{JB_ja_tasks}->{element}}) {
  if (ref $i->{JAT_granted_destin_identifier_list}->{element} eq 'HASH') {
    $i->{JAT_granted_destin_identifier_list}->{element} = [$i->{JAT_granted_destin_identifier_list}->{element}];
  }
  if (ref $i->{JAT_scaled_usage_list}->{element} eq 'HASH') {
    $i->{JAT_scaled_usage_list}->{element} = [$i->{JAT_scaled_usage_list}->{element}];
  }
  if ($i->{JAT_state} ne '128') {
     # We are only interested in running tasks (128 == 'r')!
     # print "$mpi,$JB_owner,$i->{JAT_task_number},JAT_state $i->{JAT_state}\n";
     next;
  }
  foreach $j (@{$i->{JAT_granted_destin_identifier_list}->{element}}) {
        
    $CoreByNode{$j->{JG_qname}} += $j->{JG_slots};
    $CoreByUser{$JB_owner} += $j->{JG_slots};
    if (ref $i->{JAT_granted_request_list}->{element} eq 'HASH') {
       $i->{JAT_granted_request_list}->{element} = [$i->{JAT_granted_request_list}->{element}];
    }
    foreach $k (@{$i->{JAT_granted_request_list}->{element}}) {
      if ($k->{CE_name} eq "h_rt") {
        $RtByUser{$JB_owner} += $k->{CE_stringval} * $j->{JG_slots};
        $RtByNode{$j->{JG_qname}} += $k->{CE_stringval} * $j->{JG_slots};
      } elsif ($k->{CE_name} eq "h_vmem") {
        $j->{CE_stringval}  =~ tr/mg/MG/;
        $MemByUser{$JB_owner} += (unformat_number($k->{CE_stringval}) * $j->{JG_slots});
        #$JG_qname=$j->{JG_qname}; # Need to remember qname
        $MemByNode{$j->{JG_qname}} += (unformat_number($k->{CE_stringval}) * $j->{JG_slots});
      }
    }
  
    if ($opt_n || $opt_M) { # If we checking ioops maxvmem etc for all jobs (-n -M)
      foreach $k (@{$i->{JAT_scaled_usage_list}->{Events}}) {
          if ($k->{UA_name} eq 'ioops') {
              #print "$JB_owner $j->{JG_qname} $mpi $k->{UA_value}\n";
              $ioopsByUser{$JB_owner} += $k->{UA_value};
              $ioopsByNode{$j->{JG_qname}} += $k->{UA_value};
          }
          elsif ($k->{UA_name} eq 'iow') {
              #print "$JB_owner $mpi iow $k->{UA_value}\n";
              $iowByUser{$JB_owner} += $k->{UA_value};
              $iowByNode{$j->{JG_qname}} += $k->{UA_value};
          }
          elsif ($k->{UA_name} eq 'io') {
              #print "$JB_owner $mpi io $k->{UA_value}\n";
              $ioByUser{$JB_owner} += $k->{UA_value};
              $ioByNode{$j->{JG_qname}} += $k->{UA_value};
          } elsif ($k->{UA_name} eq 'maxvmem') {
              # print "$JB_owner $mpi maxvmem $k->{UA_value}\n";
              $maxvmemByUser{$JB_owner} += $k->{UA_value};
              $maxvmemByNode{$j->{JG_qname}} += $k->{UA_value};
         } 
      }
    }    
  }
 }
}

if ($opt_n && !$opt_g && !$opt_q && !$opt_Q) {
   printf "      ioops          io    iow User\n";
   foreach my $user (sort {$ioopsByUser{$a} <=> $ioopsByUser{$b}} keys %ioopsByUser) {
      printf "%13d %9d %6d %s\n", $ioopsByUser{$user}, $ioByUser{$user}, $iowByUser{$user}, $user;
        $Totalioops += $ioopsByUser{$user};
  }
  printf "%13d TOTAL ioops\n\n", $Totalioops;
}


if (($opt_c || $opt_V) && !$opt_g && !$opt_q && !$opt_Q && !($opt_V && ($opt_w || $opt_W))) {
   if ($opt_c) {
     printf "     Cores User\n";
     foreach my $user (sort {$CoreByUser{$a} <=> $CoreByUser{$b}} keys %CoreByUser) {
           printf "%9d %s\n", $CoreByUser{$user}, $user;
	   $TotalCores += $CoreByUser{$user};
     }
     printf "%9d TOTAL Cores\n\n", $TotalCores;
   } elsif ($opt_V) {
     printf "     GPUs User\n";
     foreach my $user (sort {$GPUbyUser{$a} <=> $CoreByUser{$b}} keys %GPUbyUser) {
           printf "%9d %s\n", $GPUbyUser{$user}, $user;
           $TotalGPUs += $GPUbyUser{$user};
     }
     printf "%9d TOTAL GPUs\n\n", $TotalGPUs;
   }
}
        
if ($opt_m && !$opt_g && !$opt_q && !$opt_Q) {
 if ($opt_M) {
   printf "   Memory  Maxvmem     Mem/Core    maxvmem/Core        User\n"
 } else {
   printf "   Memory  Mem/Core User\n";
 }
 foreach my $user (sort {$MemByUser{$a} <=> $MemByUser{$b}} keys %MemByUser) {
        $memory = $MemByUser{$user} / 1073741274;
        $mempercore = $memory / $CoreByUser{$user};
        $maxvmem = $maxvmemByUser{$user} / 1073741274;
        $maxvmempercore = $maxvmem / $CoreByUser{$user};
        $TotalMemory += $memory;
        $Totalmaxvmem += $maxvmem;
        if ($opt_M) {
 	   printf "%9.2f  %7.2f     %7.2f          %7.2f     %s\n", $memory, $maxvmem, $mempercore, $maxvmempercore, $user;
        } else {
 	   printf "%9.2f  %6.2f   %s\n", $memory, $mempercore, $user;
        }
  }
  printf "%9.2f   TOTAL Memory (G)\n", $TotalMemory;
  printf "%9.2f   TOTAL maxvmem (G)\n", $Totalmaxvmem if ($opt_M);
  printf "\n";
}

if ($opt_r && !$opt_g) {
  printf "  RunTime   RT/Core User\n";
  foreach my $user (sort {$RtByUser{$a} <=> $RtByUser{$b}} keys %RtByUser) {
        $RtPerCore = $RtByUser{$user} / $CoreByUser{$user} / 86400;
        $Rt = $RtByUser{$user} / 86400;
        printf "%9.2f %9.2f %s\n", $Rt, $RtPerCore, $user;
        $TotalRt += $Rt;
  }
  printf "%9.2f     TOTAL RT (Days)\n\n", $TotalRt;
}

if ($opt_t && !$opt_g) {
my $TotalCores, $memory, $mempercore, $TotalMemory, $RtPerCore, $Rt, $TotalRt;
  foreach my $user (sort {$CoreByUser{$a} <=> $CoreByUser{$b}} keys %CoreByUser) {
    $TotalCores += $CoreByUser{$user};
    $memory = $MemByUser{$user} / 1073741274;
    $mempercore = $memory / $CoreByUser{$user}; 
    $TotalMemory += $memory;
    $RtPerCore = $RtByUser{$user} / $CoreByUser{$user} / 86400;
    $Rt = $RtByUser{$user} / 86400;
    $TotalRt += $Rt;
    printf "%s %9d %9.2f %6.2f %9.2f %9.2f\n", $user, $CoreByUser{$user}, $memory, $mempercore, $Rt, $RtPerCore;
  }
  printf "Total %9d %6.2f %9.2f\n\n", $TotalCores, $TotalMemory, $TotalRt; 
}

if ($opt_c && $opt_g) {
 printf "Node Core\n";
 foreach my $node (sort keys %CoreByNode) {
        $core = $CoreByNode{$node};
        $node =~ s/.*\@node//;
        $node =~ s/.*\@//;
	$node =~ s/\..*//;
	printf "%s %3d ", $node, $core;
	for ($i=0; $i < $core; $i++){ print"#"};for (; $i < $MaxCore{$node}; $i++){ print" "};
        if ($opt_G) {
           printf "|%d\n", $MaxCore{$node};
        } else {
           print "|\n";
        }

 }
 print "\n";
}

if ($opt_m && $opt_g) {
 printf "Node Mem (G)\n";
 foreach my $node (sort keys %MemByNode) {
        $memory = $MemByNode{$node};
        $node =~ s/.*\@node//;
        $node =~ s/.*\@//;
	$node =~ s/\..*//;
        $memory = format_bytes($memory, bs=>1024);
	printf "%s %3d ", $node, $memory;
	$memory =~ s/G//;
	$memory =~ s/M//;
        if ($MaxCore{$node} != 0) {
          $memPerCore = $MaxMem{$node}/ $MaxCore{$node};
	  $count = int ($memory / $memPerCore);
        }
        #print "MemByNode=$MemByNode{$node} memory=$memory count=$count memPerCore=$memPerCore\n";
	for ($i=0; $i < $count; $i++){ print"#"};for (; $i < $MaxCore{$node}; $i++){ print" "}; 
        if ($opt_G) {
           printf "|%.1f\n", $MaxMem{$node}; 	
        } else {
           print "|\n";
        }
 }
}

if ($opt_n && $opt_g) {
   printf "Node        ioops\n";
   foreach my $node (sort keys %ioopsByNode) {
        $ioops = $ioopsByNode{$node};
        $node =~ s/.*\@node//;
        $node =~ s/.*\@//;
        $node =~ s/\..*//;
        printf "%s %12d ", $node, $ioops;
        $count = int(log($ioops)/log(10));
        for ($i=0; $i < $count; $i++){ print"###"};
           print "\n";
   }
}

if ($opt_w || $opt_W) {
  # get the UUNs of IGC users (not needed now as we are only looking at jobs waiting for IGC resources)
  #open(USERS,   "qconf -su igmm-cluster |") || die("can't open input: $!");
  #while (<USERS>) {
    #$_ =~ s/[ \\]//g; 
    #@i = split(/,/,$_); 
    #$igmmusers{$_}++ for (@i);
  #}
          if (!$opt_q && !$opt_Q) {
            if ($opt_V) {
              print "Job-ID   Job-Name   User     Submit-Time         GPUs Cores   Tasks  Mem/Core  Runtime (Days)\n";
            } else {
              print "Job-ID   Job-Name   User     Submit-Time         Cores   Tasks  Mem/Core  Runtime (Days)\n";
   	    }
           }   
	  foreach $i ($data->{job_info}->{job_list}) {
            $i = [$i] if (ref $i eq 'HASH');
	    foreach $j (sort {$a->{JB_job_number} <=> $b->{JB_job_number}} @$i) {

              next if ($j->{state}[1] ne "qw");
              next if ($j->{requested_pe}->{name} ne "gpu-a100" && $opt_V);
              next if defined $UsersWithRuningJobs{$j->{JB_owner}} && $opt_W;
              next if (! defined($igmmusers{$j->{JB_owner}}) && $opt_U);
              $j->{JB_submission_time} =~ s/T/ /;
              $j->{JB_submission_time} =~ s/\..*//;
              if ($j->{tasks} =~ /(\d+)-(\d+):(\d+)/) {
	        $tasks = ($2-$1+1)/$3;
              } else {
                $tasks = 1;
              }
              $j->{JB_name} = substr($j->{JB_name}, 0, 10);
              if (!$opt_q && !$opt_Q) {
		 if ($opt_V) {
                    printf "%s %-10s %8s %s %4d %4d %8d", $j->{JB_job_number}, $j->{JB_name}, $j->{JB_owner}, $j->{JB_submission_time}, $j->{requested_pe}->{content}, $j->{slots}, $tasks;
                 } else {
                    printf "%s %-10s %8s %s %4d %8d", $j->{JB_job_number}, $j->{JB_name}, $j->{JB_owner}, $j->{JB_submission_time}, $j->{slots}, $tasks;
                 }
              }
              $WaitingCoreByUser{$j->{JB_owner}} += $j->{slots} * $tasks;
              if ($j->{hard_request}->{h_vmem}->{content} eq "") {
		$j->{hard_request}->{h_vmem}->{content} = "$DefaultMemPerCore";
              }
              $j->{hard_request}->{h_vmem}->{content} = unformat_number ($j->{hard_request}->{h_vmem}->{content}) / 1073741274;
              $j->{hard_request}->{h_rt}->{content} /= 86400;
              $WaitingMemoryByUser{$j->{JB_owner}} += ($j->{slots} * $tasks * $j->{hard_request}->{h_vmem}->{content});

              if ( $j->{requested_pe}->{name} eq "gpu-a100") {
                $WaitingGPUbyUser{$j->{JB_owner}} += $j->{requested_pe}->{content};
              } elsif ($j->{hard_request} eq "gpu" || $j->{queue_name} =~ /gpu/) {
                $WaitingGPUbyUser{$j->{JB_owner}}++;
              }

              if (!$opt_q && !$opt_Q) {
                printf "%7.2f %9.2f\n", $j->{hard_request}->{h_vmem}->{content}, $j->{hard_request}->{h_rt}->{content};
              }
            }
          }
          print "\n";
          if ($opt_q || $opt_Q) {
           if($opt_c) {
             printf "Cores Waiting  Running User\n";
             foreach my $user (sort {$WaitingCoreByUser{$a} <=> $WaitingCoreByUser{$b}} keys %WaitingCoreByUser) {
                printf "%9d %9d    %s\n", $WaitingCoreByUser{$user}, $CoreByUser{$user}, $user;
                $TotalWaitingCores += $WaitingCoreByUser{$user};
             }
             printf "%9d TOTAL Waiting Cores\n\n", $TotalWaitingCores;
           } elsif ($opt_V) {
             printf "GPUs Waiting  Running User\n";
             foreach my $user (sort {$WaitingGPUbyUser{$a} <=> $WaitingGPUbyUser{$b}} keys %WaitingGPUbyUser) {
                printf "%9d %9d    %s\n", $WaitingGPUbyUser{$user}, $GPUByUser{$user}, $user;
                $TotalWaitingGPU += $WaitingGPUbyUser{$user};
             }
             printf "%9d TOTAL Waiting GPUs\n\n", $TotalWaitingGPU;
                
           } elsif ($opt_m) {
             printf "Memory Waiting  Running User\n";
             foreach my $user (sort {$WaitingMemoryByUser{$a} <=> $WaitingMemoryByUser{$b}} keys %WaitingMemoryByUser) {
                $memory = $MemByUser{$user} / 1073741274;
                printf "%9.2d %9.2d    %s\n", $WaitingMemoryByUser{$user}, $memory, $user;
                $TotalWaitingMemory += $WaitingMemoryByUser{$user};
             }
             printf "%9.2d TOTAL Waiting Memory\n\n", $TotalWaitingMemory;
          } 
      }
}

sub getjobXML {
    # given a numeric job ID return a reference to an job XML data structure 
    my $jobid = $_[0]; 
    my $age = (-M "$qwhodir/$jobid.xml") * 86400;
    my $xml = new XML::Simple;
    system ("qstat  -j $jobid -xml > $qwhodir/$jobid.xml") if (!-e "$qwhodir/$jobid.xml" || $age > $CacheTTL || $opt_f);
    if (!-e "$qwhodir/jobid.sto" || $age > $CacheTTL || $opt_f) {
	  print "Creating $qwhodir/$jobid.xml $age $CacheTTL $opt_f\n";
          $jobdata = $xml->XMLin("$qwhodir/$jobid.xml");
          # write Perl Data Structure (binary Storable file)
          store($jobdata, "$qwhodir/$jobid.sto");
    } else {
          $jobdata = $xml->XMLin("$qwhodir/$mpi.xml");
    }

    # print $mpidata->{queue_info}->{job_list};
    # Dump Perl mpidata structures
    if ($opt_D) {
           $DUMP = "> $qwhodir/$jobid.dump";
           open DUMP or die "Can't open: $qwhodir/$mpi.dump: $!";
           print DUMP Dumper($mpidata);
           close DUMP;
   }
   return $mpidata;
}

        
=head1 NAME

qwho - show who is using the IGC Eddie nodes along with the amout of resource allocated (core, memory, run-time).

=head1 SYNOPSIS

qwho [-aisDfcmMrgGhnqQ] 

=head1 DESCRIPTION

B<qwho> show who is using the IGC Eddie nodes along with the amout of resource allocated (core, memory, run-time).
The -t option prints a table showing User, Core, Memory, Memory-per-Core, Run-Time, Run-Time-Per-Core - can be 
used to produce graphs using Grafana etc.

=head1 OPTIONS

=over

=item -h

Print help.

=item -a

Show resoures usage for all Eddie nodes.

=item -u

Show resoures usage for UoE Eddie nodes accessable to all UoE researchers.

=item -U

Only consider IGC users.  Normally used with -aU or -uU to show IGC users running on UoE nodes or all nodes.

=item -c

Show core allocation (default).

=item -m

Show total memory allocation and memory per core.

=item -M

As -m, but also show the current running maxvmem for each user. This option is slow as an XML file is generadted and parsed for each job.

=item -r 

Show run-time allocation and run-time per core.

=item -g

Print "node graph" of either cores or memory or network.  Used in conjunction with -c or -m or -n. For memory (-m), the hash blocks are in units of memory-per-core - so the size of a memory hash block may vary from node to node. For network (-n) 3 hash blocks represent a unit step the decimal log of the ioops. eg if ioops=1000, then log(1000)=3, so 3x3=9 #'s are displayed.

=item -G

As -g, but also print either max possible cores or memory for each node.  Used in conjunction with -c or -m.

=item -i

Show interactive nodes only.

=item -n

Show "network" activity - currently shows total ioops io and iow per user and ioops per node when used with the -g option. This option is slow as an XML file is generadted and parsed for each job.

=item -s

Show staging nodes only.

=item -t

Print a table of resource usage per user and total for core memory and runtime.

=item -V

Only consider jobs/tasks that have requested or have been allocated a NVIDIA A100 GPU

=item -w

List jobs that are waiting, that is 'pending' and 'qw'.

=item -W

As -w, but only show users with waiting jobs who have no jobs or tasks currently running on IGC nodes. This gives a measure of how fair or unfair current user resource limits may be.

=item -q -Q

As -w and -W, but show the number of waiting cores per user (for just those users waiting for cores) and the number of running cores (if any) they have running.  -Q only shows those users with no running cores.

=item -f 

Force new cached copy of system state (saved in ~/.qwho).  The cache time-to-live is 5 minutes.

=item -D

Dump a text representation of the interal Perl data structures to '.dump' files in '~/.qwho'.  Useful for debugging and development.

=item -v

Print version.

=back

=head1 BUGS

qwho generates many files in ~/.qwho, so a regular clear out might be nessary.

=head1 AUTHOR

John Ireland - UoE IGC - May 2021
