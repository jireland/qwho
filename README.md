# NAME
    qwho - show who is using the IGC Eddie nodes along with the amout of
    resource allocated (core, memory, run-time).

# SYNOPSIS
    qwho [-aisDfcmMrgGhnqQ]

# DESCRIPTION
    qwho show who is using the IGC Eddie nodes along with the amout of
    resource allocated (core, memory, run-time). The -t option prints a
    table showing User, Core, Memory, Memory-per-Core, Run-Time,
    Run-Time-Per-Core - can be used to produce graphs using Grafana etc.

# OPTIONS
    -h  Print help.

    -a  Show resoures usage for all Eddie nodes.

    -u  Show resoures usage for UoE Eddie nodes accessable to all UoE
        researchers.

    -c  Show core allocation (default).

    -m  Show total memory allocation and memory per core.

    -M  As -m, but also show the current running maxvmem for each user. This
        option is slow as an XML file is generadted and parsed for each job.

    -r  Show run-time allocation and run-time per core.

    -g  Print "node graph" of either cores or memory or network. Used in
        conjunction with -c or -m or -n. For memory (-m), the hash blocks
        are in units of memory-per-core - so the size of a memory hash block
        may vary from node to node. For network (-n) 3 hash blocks represent
        a unit step the decimal log of the ioops. eg if ioops=1000, then
        log(1000)=3, so 3x3=9 #'s are displayed.

    -G  As -g, but also print either max possible cores or memory for each
        node. Used in conjunction with -c or -m.

    -i  Show interactive nodes only.

    -n  Show "network" activity - currently shows total ioops io and iow per
        user and ioops per node when used with the -g option. This option is
        slow as an XML file is generadted and parsed for each job.

    -s  Show staging nodes only.

    -t  Print a table of resource usage per user and total for core memory
        and runtime.

    -w  List jobs that are waiting, that is 'pending' and 'qw'.

    -W  As -w, but only show users with waiting jobs who have no jobs or
        tasks currently running on IGC nodes. This gives a measure of how
        fair or unfair current user resource limits may be.

    -q -Q
        As -w and -W, but show the number of waiting cores per user (for
        just those users waiting for cores) and the number of running cores
        (if any) they have running. -Q only shows those users with no
        running cores.

    -f  Fast mode. Uses cached copy of system state (saved in ~/.qwho). The
        cache time-to-live is 5 minutes.

    -D  Dump a text representation of the interal Perl data structures to
        '.dump' files in '~/.qwho'. Useful for debugging and development.

    -v  Print version.

# AUTHOR
    John Ireland - UoE IGC - May 2021

